//! Communication interface implementations for `embedded_hal` version 0.2

pub mod i2c;
pub mod spi;
